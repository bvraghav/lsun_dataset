SHELL		:= /usr/bin/zsh

LSUN_ROOT	:= .

CAT_URL		:= http://dl.yf.io/lsun/categories.txt
SCENES		:= $(shell curl -sL '${CAT_URL}')

ROOT_URL	:= http://dl.yf.io/lsun
TRAIN_DBS	:= ${SCENES:%=${LSUN_ROOT}/scenes/%_train_lmdb}
VAL_DBS		:= ${SCENES:%=${LSUN_ROOT}/scenes/%_val_lmdb}
TEST_DBS	:= ${LSUN_ROOT}/scenes/test_lmdb

OBJECTS :=
OBJECTS += airplane bicycle bird boat bottle
OBJECTS += bus car cat chair cow
OBJECTS += dining_table dog horse motorbike person
OBJECTS += potted_plant sheep sofa train tv-monitor

OBJECT_DBS	:= ${OBJECTS:%=${LSUN_ROOT}/objects/%}

# Num connections for parallel download.
# Heuristically:
# 8 -> 1.3MBps, 16 -> 2.5MBps, 32 -> 5MBps, 64 -> 10MBps
# Caps at 19MBps with 128 connections after
# 10GB of downloads
NUM_CONN	:= 128

download-curl: download := curl -sLO
download-axel: download := axel -can ${NUM_CONN}

download: download-axel

download-axel download-curl : ${LSUN_ROOT} ${TRAIN_DBS} ${VAL_DBS} ${TEST_DBS} ${OBJECT_DBS}

${LSUN_ROOT}/objects/% ${LSUN_ROOT}/scenes/%_lmdb :
	[ -d $@ ] || mkdir -p $@
	cd ${@D} ; $(download) ${ROOT_URL}/${@:${LSUN_ROOT}/%=%}.zip
	unzip ${@F}.zip
