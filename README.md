# LSUN

Please check [LSUN webpage](http://www.yf.io/p/lsun) for more information about the dataset.


### Download data
Please make sure you have `cURL`/`axel` installed
```bash
# with axel
make download
```

To download with `cURL`, comment out the axel part and
uncomment the curl part in the Makefile, and,
```sh
# with curl uncommented and axel commented,
make download -j 32
```

## Data Release

All the images in one category are stored in one lmdb database
file. The value
 of each entry is the jpg binary data. We resize all the images so
 that the
  smaller dimension is 256 and compress the images in jpeg with
  quality 75.
  
### Citing LSUN

If you find LSUN dataset useful in your research, please consider citing:

    @article{yu15lsun,
        Author = {Yu, Fisher and Zhang, Yinda and Song, Shuran and Seff, Ari and Xiao, Jianxiong},
        Title = {LSUN: Construction of a Large-scale Image Dataset using Deep Learning with Humans in the Loop},
        Journal = {arXiv preprint arXiv:1506.03365},
        Year = {2015}
    }
